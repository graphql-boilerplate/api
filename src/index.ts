require('dotenv').config();

import { GraphQLServer } from 'graphql-yoga';
import { Prisma } from 'prisma-binding';
import { resolvers } from './resolvers';
import { permissions } from './permissions';
import * as Email from 'email-templates';
import { graphqlAuthenticationConfig } from 'graphql-authentication';
import { GraphqlAuthenticationPrismaAdapter } from 'graphql-authentication-prisma';

const mailer = new Email({
  message: {
    from: process.env.FROM_EMAIL
  }
});

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  resolverValidationOptions: {
    requireResolversForResolveType: false
  },
  middlewares: [permissions],
  context: req => ({
    ...req,
    db: new Prisma({
      typeDefs: './src/prisma.graphql',
      endpoint: process.env.PRISMA_ENDPOINT
    }),
    graphqlAuthentication: graphqlAuthenticationConfig({
      adapter: new GraphqlAuthenticationPrismaAdapter(),
      secret: process.env.APP_SECRET,
      mailer,
      mailAppUrl: process.env.MAIL_APP_URL
    })
  })
});
server.start({ port: process.env.PORT }, () =>
  console.log(`Server is running on http://localhost:${process.env.PORT}`)
);
