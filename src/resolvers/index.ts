import { mergeResolvers } from 'merge-graphql-schemas';
import { UserResolver } from './UserResolver';

const resolversList = [UserResolver];

export const resolvers = mergeResolvers(resolversList);
