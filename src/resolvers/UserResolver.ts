import { authQueries, authMutations } from 'graphql-authentication';
import { OAuth2Client } from 'google-auth-library';
import * as jwt from 'jsonwebtoken';
import * as crypto from 'crypto';

const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);

const getGoogleUser = async (googleToken: string) => {
  const ticket = await client.verifyIdToken({
    idToken: googleToken,
    audience: process.env.GOOGLE_CLIENT_ID
  });

  return ticket.getPayload();
};

const generateToken = (user, context) => {
  return jwt.sign({ userId: user.id }, context.graphqlAuthentication.secret);
};

export const UserResolver = {
  Query: {
    ...authQueries
  },
  Mutation: {
    ...authMutations,
    async googleAuthentication(_, args, context, info) {
      const googleUser = await getGoogleUser(args.googleToken);
      const currentDate = new Date().toISOString();
      let user = await context.graphqlAuthentication.adapter.findUserByEmail(
        context,
        googleUser.email
      );

      if (!user) {
        user = context.db.mutation.createUser(
          {
            data: {
              name: googleUser.name,
              email: googleUser.email,
              password: crypto.randomBytes(16).toString('hex'),
              picture: googleUser.picture,
              googleUserId: googleUser.sub,
              joinedAt: currentDate,
              lastLogin: currentDate
            }
          },
        );
      } else {
        await context.graphqlAuthentication.adapter.updateUserLastLogin(
          context,
          user.id,
          {
            lastLogin: currentDate
          }
        );
      }

      return {
        token: generateToken(user, context),
        user
      };
    }
  }
};
