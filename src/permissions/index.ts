import { rule, shield } from 'graphql-shield';
import { isAuthResolver } from 'graphql-authentication';

const genericRules = {
  isAuthenticated: rule()(isAuthResolver)
};

const rules = {
  ...genericRules
};

export const permissions = shield({
  Mutation: {}
});
